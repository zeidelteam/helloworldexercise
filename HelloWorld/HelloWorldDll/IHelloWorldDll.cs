﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HelloWorldDll
{
    public interface IHelloWorldDll
    {
        string Message
        {
            set;
        }
        string GetMessage();
        string GetMessage(string sOutFile);
        string GetDest();
    }
}
