﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;

namespace HelloWorldDll
{
    public class clsHelloWorldDll : IHelloWorldDll 
    {
        private string _Message = "Hello World!";

        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string GetMessage()
        {
            return _Message;
        }
        public string GetMessage(string sOutFile)
        {
            StreamWriter sw = new StreamWriter(sOutFile);
            sw.WriteLine(_Message);
            sw.Close();
            sw.Dispose();
            return _Message;
        }

        public string GetDest()
        {
            string sDest = ConfigurationManager.AppSettings.Get("Destination");
            return sDest;
        }

    }


}
