﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HelloWorldDll;

namespace HelloWorld
{
    class HelloWorld
    {
        static void Main(string[] args)
        {
            clsHelloWorldDll dll = new clsHelloWorldDll();
            string sDest = dll.GetDest();
            if (sDest == "FILE")
            {
                System.Console.WriteLine(dll.GetMessage("OutFile.txt"));
            }
            else
            {
                System.Console.WriteLine(dll.GetMessage());
            }

        }
    }
}
